# Archivematica Helm

## How it works? 

This project deploys: [archivematica](https://www.archivematica.org/pt-br/docs/archivematica-1.13/), [archivematica-storage](https://www.archivematica.org/pt-br/docs/storage-service-0.19/) and mysql on a kubernetes cluster. In addiction, it runs all needed migrations and create an admin user on storage user. Docker images used on this project can be found [here](https://gitlab.com/rdc-arq/archivematica) and [here](https://gitlab.com/rdc-arq/archivematica-storage)

## How to use?

- helm repo add archivematica https://gitlab.com/api/v4/projects/37677650/packages/helm/stable
- helm repo update
- helm install -f values.yml archivematica/archivematica --generate-name (-n your namespace here, it you don't want to deploy on default)

Obs. You need to pick values.yml and change values as needed. 
